# PWA de Hamburgueria

## O que é um PWA ???

Aplicativo Web que entrega a experiência de uso de app comum para os usuários usando as funcionalidades providas pelos recursos das páginas web e seus browsers. No fim verá que é apenas um website que é executado com alguns recursos. 
O PWA nos dá a opção de:

 - Instalar o sistema na tela do celular
 - Acessar o mesmo offline
 - Acessar a câmera
 - Enviar notificações
 - Fazer atualização em background
